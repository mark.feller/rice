export GCLOUD_SDK="$HOME/prog/google-cloud-sdk"
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/mfeller/prog/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/mfeller/prog/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/mfeller/prog/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/mfeller/prog/google-cloud-sdk/completion.zsh.inc'; fi
